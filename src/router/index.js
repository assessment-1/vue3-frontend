import Vue from "vue";
import VueRouter from "vue-router";
// import Home from "../views/Home.vue";
import Login from '../views/Login.vue';
import Register from '../views/Register.vue';
import Forget from '../views/Forget.vue';
import PageNotFound from '../views/PageNotFound.vue';
import DashboardHome from '../views/dashboard/home.vue';
import NewPassword from '../views/ForgotForm.vue';
import EditUser from '../views/dashboard/edit.vue';
// import Photos from '../compo

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
  },
  {
    path: "/forget",
    name: "forget",
    component: Forget,
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: DashboardHome,
  },
  {
    path: "/dashboard/edit",
    name: "New Password",
    component: EditUser,
  },
  {
    path: "/new-password",
    name: "New Password",
    component: NewPassword,
  },
  
  { path: "*", component: PageNotFound },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});
 
export default router;
